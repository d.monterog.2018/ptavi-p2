

# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija
import csv


with open(sys.argv[1]) as fichero:  # Abrimos el fichero que deseamos
    lectura = csv.reader(fichero)  # Separamos las lineas del fichero

    for linea in lectura:

        operadores = linea[0]  # Marca el tipo de operacion que vamos ha hacer
        valortemporal = int(linea[1])  # Actualiza el valor al salir del bucle

        for numero in linea[2:]:

            operador = int(numero)  # Recorre los numeros de la lista
            operacion = calcoohija.CalculadoraHija(valortemporal, operador)

            if operadores == "suma":
                valortemporal = operacion.suma()

            elif operadores == "resta":
                valortemporal = operacion.resta()

            elif operadores == "multiplicacion":
                valortemporal = operacion.multiplicacion()

            elif operadores == "division":
                valortemporal = operacion.division()

            else:
                sys.exit('Calculadora solo suma, resta, multiplica y divide.')
        print("Estas realizando una " + operadores + ", y el valor total es: ")
        print(valortemporal)
