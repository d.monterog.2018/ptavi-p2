

# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multiplicacion(self):
        return self.primero * self.segundo

    def division(self):
        try:
            return self.primero / self.segundo
        except ZeroDivisionError:
            print("Error: No se puede dividir entre 0")


if __name__ == "__main__":

    try:
        numero1 = int(sys.argv[1])
        operando = sys.argv[2]
        numero2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    objeto = CalculadoraHija(numero1, numero2)

    if operando == "suma":
        result = objeto.suma()
    elif operando == "resta":
        result = objeto.resta()
    if operando == "multiplicacion":
        result = objeto.multiplicacion()
    elif operando == "division":
        result = objeto.division()
    else:
        sys.exit('La calculadora solo suma, resta, multiplica y divide.')

    print(result)
