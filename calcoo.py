

# !/usr/bin/python3
# -*- coding: utf-8 -*-


import sys


class Calculadora():

    def __init__(self, numero1, numero2):
        self.primero = numero1
        self.segundo = numero2

    def suma(self):
        return self.primero + self.segundo

    def resta(self):
        return self.primero - self.segundo


if __name__ == "__main__":
    try:
        numero1 = int(sys.argv[1])
        operando = sys.argv[2]
        numero2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    objeto = Calculadora(numero1, numero2)

    if operando == "suma":
        result = objeto.suma()
    elif operando == "resta":
        result = objeto.resta()
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
